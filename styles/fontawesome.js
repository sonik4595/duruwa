import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faBars,
  faCopyright,
  faBalanceScale,
  faPiggyBank,
  faRoute,
  faBinoculars
} from "@fortawesome/free-solid-svg-icons";
import { faGoogle } from "@fortawesome/free-brands-svg-icons";

library.add(
  faBars,
  faGoogle,
  faCopyright,
  faBalanceScale,
  faPiggyBank,
  faRoute,
  faBinoculars
);